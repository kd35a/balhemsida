class Preference < ActiveRecord::Base
  belongs_to :attendee

  validates :medal, inclusion: {in: ['yes', 'trade_in', 'no']}
  enum ticket_type: [:student, :non_student, :giver, :active]
  validates :ticket_type, inclusion: { in: ticket_types.keys }

  cattr_reader :prices

  @@prices = {
    :student => 725,
    :non_student => 845,
    :active => 575,
    :giver => 1000,
    :alcohol => 50,
    :sexa => 100,
    :medal => 100,
    :trade_in => 50,
    :lunch => 90,
    :booklet => 100
  }

  def get_price()
    price = {}

    price[:dinner] = @@prices[self.ticket_type.to_sym]
    price[:dinner] += self.alcohol ? @@prices[:alcohol] : 0
    price[:sexa] = @@prices[:sexa] if self.sexa
    price[:medal] = @@prices[:medal] if self.medal == 'yes'
    price[:medal] = @@prices[:trade_in] if self.medal == 'trade_in'
    price[:lunch] = @@prices[:lunch] if self.lunch
    price[:booklet] = @@prices[:booklet] if self.booklet
    price[:total_price] = price.values.reduce(:+)
    return price
  end
end
