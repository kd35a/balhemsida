class Foodpreference < ActiveRecord::Base
  belongs_to :attendee
  validate :only_one_veg_option

  def only_one_veg_option
    if (vegetarian && vegan)
      errors[:vegan] << I18n.t(:both_veg_error)
      errors[:vegetarian] << I18n.t(:both_veg_error)
    end
  end
end
