class Attendee < ActiveRecord::Base
  has_one :preference, dependent: :destroy
  has_one :foodpreference, dependent: :destroy
  accepts_nested_attributes_for :preference
  accepts_nested_attributes_for :foodpreference

  validates :name, presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, uniqueness: true,
    format: { with: VALID_EMAIL_REGEX }
  validates :phone, presence: true
  validates :address, presence: true
  validates :pnbr, presence: true,
    length: {is: 10, message: "format YYMMDDXXXX"}, uniqueness: true
  validates :status, inclusion: {in: ['attending', 'reserve', 'removed']}
  validates :previous_visits, numericality: {allow_blank: true}

  validates :preference, presence: true
  validates :foodpreference, presence: true

  before_save {self.email = email.downcase}
  before_create :generate_refnbr

  def row_color
    if self.status == 'reserve'
      :none
    elsif self.paid
      :green
    else
      :red
    end
  end

  protected

  def generate_refnbr
    range = 10000..100000
    self.refnbr = loop do
      random_refnbr = rand(range)
      break random_refnbr.to_s unless Attendee.exists?(refnbr: random_refnbr.to_s)
    end
  end

end
