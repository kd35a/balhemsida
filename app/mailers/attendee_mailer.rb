class AttendeeMailer < ActionMailer::Base
  default return_path: "bal-noreply@blekingska.se",
          from: "bal@blekingska.se"
  layout 'email'
  helper AttendeeHelper

  @@organizer_receivers = [
    'bal@blekingska.se'
  ]
  @@tech_receivers = [
    'fredrik@strandin.name'
  ]
  @@subject_tag = '[bal.blekingska.se]'

  def attendee(attendee)
    @attendee = attendee
    @is_giver = attendee.preference.ticket_type.to_sym == :giver
    mail to: attendee.email
  end

  def reserve(attendee)
    @attendee = attendee
    @is_giver = attendee.preference.ticket_type.to_sym == :giver
    mail to: attendee.email
  end

  def organizer(attendee)
    @attendee = attendee
    @registrations = Attendee.where(status: 'attending').count
    @attendees_not_paid = Attendee.where(paid: false, status: 'attending').count
    @reserves = Attendee.where(status: 'reserve').count

    I18n.with_locale(:sv) do
      mail to: @@organizer_receivers,
           cc: @@tech_receivers,
           subject: "#{@@subject_tag} Ny anmälan från gäst"
    end
  end

  def organizer_reserve(attendee)
    @attendee = attendee
    @registrations = Attendee.where(status: 'attending').count
    @attendees_not_paid = Attendee.where(paid: false, status: 'attending').count
    @reserves = Attendee.where(status: 'reserve').count

    I18n.with_locale(:sv) do
      mail to: @@organizer_receivers,
           cc: @@tech_receivers,
           subject: "#{@@subject_tag} Ny person på reservlistan"
    end
  end

  def paid_notice(attendee)
    @attendee = attendee
    I18n.with_locale(:sv) do
      mail to: attendee.email
    end
  end

  def sexa_limit_notice
    I18n.with_locale(:sv) do
      mail to: 'q@blekingska.se',
           cc: @@tech_receivers,
           subject: "#{@@subject_tag} OBS! Antalet sexagäster är nu på gränsen"
    end
  end

end
