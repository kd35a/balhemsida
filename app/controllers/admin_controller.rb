class AdminController < ApplicationController
  before_action :signed_in_user

  def index
    @registrations = Attendee.where(status: 'attending').count
    @attendees_not_paid = Attendee.where(paid: false, status: 'attending')
    @reserves = Attendee.where(status: 'reserve').count
  end

  def export_attendees
    @attendees = Attendee.where(status: 'attending')
    @reserves = Attendee.where(status: 'reserve')
    render xlsx: "export_attendees", filename: "deltagare_#{timestamp}.xlsx"
  end

  def timestamp
    Time.now.strftime '%Y-%m-%d_%H%M_%Z'
  end

end
