class StaticPagesController < ApplicationController

  def home
  end

  def history
  end

  def menu
  end

  def program
  end

  def medalj
  end

  def faq
  end

end
