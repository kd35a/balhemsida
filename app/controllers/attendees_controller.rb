class AttendeesController < ApplicationController
  before_action :signed_in_user, except: [:new, :create, :list_paid]
  include AttendeeHelper

  def show
    @attendee = Attendee.find(params[:id])
  end

  def new
    if session[:attendee_id]
      @attendee = Attendee.find(session[:attendee_id])
      @is_giver = @attendee.preference.ticket_type.to_sym == :giver
      session.delete(:attendee_id)
      if @attendee.status == 'reserve'
        render 'reserve'
      else
        render 'show'
      end
    else
      @attendee = Attendee.new
      @attendee.build_preference
      @attendee.build_foodpreference
    end
  end

  def create
    @attendee = Attendee.new(new_attendee_params)
    if sexa_cutoff_limit_reached
      @attendee.preference.sexa = false
    end
    if @attendee.save
      if use_reserve_list
        @attendee.update_attribute(:status, 'reserve')
        AttendeeMailer.reserve(@attendee).deliver
        AttendeeMailer.organizer_reserve(@attendee).deliver
      else
        AttendeeMailer.attendee(@attendee).deliver_now
        AttendeeMailer.organizer(@attendee).deliver_now
      end
      if sexa_limit_reached
        AttendeeMailer.sexa_limit_notice.deliver
      end
      session[:attendee_id] = @attendee.id
      redirect_to url_for({controller: 'attendees', action: 'new'})
    else
      render 'new'
    end
  end

  def edit_mark_as_paid
    if params.has_key?(:attendee_ids)
      @attendees_confirm = Attendee.find(params[:attendee_ids])
    else
      redirect_to url_for({controller: 'admin', action: 'index'})
    end
  end

  def update_mark_as_paid
    @attendees_updated = Attendee.find(params[:attendee_ids])
    @attendees_updated.each do |a|
      a.update_attribute(:paid, true)
      AttendeeMailer.paid_notice(a).deliver
    end
  end

  def list_paid
    @attendees = Attendee.where(paid: true, status: 'attending').order(:name)
  end

  def list_all
    @attendees = Attendee.all.order(:status, :name)
  end

  def edit
    @attendee = Attendee.find(params[:id])
  end

  def update
    @attendee = Attendee.find(params[:attendee][:id])
    if @attendee.update_attributes(edit_attendee_params)
      flash[:success] = "Deltagaren är nu uppdaterad"
      redirect_to controller: 'attendees', action: 'list_all'
    else
      render 'edit'
    end
  end

  private

  def new_attendee_params
    params.require(:attendee).permit(:name,:pnbr,:email,:address,:phone,
      :title,:companion, :previous_visits,
      preference_attributes: [:ticket_type,:alcohol,:sexa,:booklet,:medal,:lunch],
      foodpreference_attributes: [:vegan,:vegetarian,:gluten,:lactose,:other])
  end

  def edit_attendee_params
    params.require(:attendee).permit(:id,:name,:pnbr,:email,:address,:phone,
      :title,:companion,:status, :previous_visits,
      preference_attributes: [:id,:ticket_type,:alcohol,:sexa,:booklet,:medal,:lunch],
      foodpreference_attributes: [:id,:vegan,:vegetarian,:gluten,:lactose,:other])
  end

end
