module AdminHelper

  def add_attendee_worksheet(name, attendees)
    @wb.add_worksheet(name: name) do |sheet|
      columns = Attendee.column_names - ['created_at', 'updated_at']
      p_columns = Preference.column_names - ['id', 'attendee_id',
        'created_at', 'updated_at']
      fp_columns = Foodpreference.column_names - ['id', 'attendee_id',
        'created_at', 'updated_at']

      sheet.add_row columns + p_columns + fp_columns
      sheet.row_style 0, @styles[:header]

      attendees.each do |attendee|
        row = columns.map{|column| attendee[column].to_s}
        # ticket_type acts a bit weird as it is stored as an integer in the
        # database, but the accessor method maps it to the enum symbol that
        # we actually want to print (as it's human readable).
        row += p_columns.map{|column| column.eql?('ticket_type') ? attendee.preference.ticket_type : attendee.preference[column].to_s}
        row += fp_columns.map{|column| attendee.foodpreference[column].to_s}
        sheet.add_row row, style: @styles[attendee.row_color]
      end
    end
  end

end
