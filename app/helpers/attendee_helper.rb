module AttendeeHelper
  @@attendee_limit = 300
  @@sexa_limit = 165
  @@sexa_cutoff_limit = 175

  cattr_reader :sexa_limit

  def attendee_limit_reached
    Attendee.where.not(status: 'removed').count > @@attendee_limit
  end

  def use_reserve_list
    attendee_limit_reached or Time.zone.parse("2019-11-01").past?
  end

  def sexa_limit_reached
    Attendee.where(status: 'attending')
            .joins(:preference)
            .where(preferences: {sexa: true}).count == @@sexa_limit
  end

  def sexa_cutoff_limit_reached
    Attendee.where(status: 'attending')
            .joins(:preference)
            .where(preferences: {sexa: true}).count >= @@sexa_cutoff_limit
  end
end
