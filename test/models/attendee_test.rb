require 'test_helper'

class AttendeeTest < ActiveSupport::TestCase
  test "should have correct price" do
    attendee = Attendee.create(email: "test@domain.com", pnbr: "0", refnbr: "0")
    pref = attendee.create_preference(student: true, sexa: true, alcohol: true)
    assert_equal(770,pref.get_price()[:total_price],"Incorrect sum")
  end
end
