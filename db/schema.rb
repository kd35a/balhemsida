# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2016_09_18_171138) do

  create_table "attendees", force: :cascade do |t|
    t.string "name"
    t.string "pnbr", null: false
    t.string "email", null: false
    t.string "companion"
    t.string "title"
    t.string "address"
    t.string "refnbr", null: false
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "paid", default: false, null: false
    t.string "status", default: "attending"
    t.string "previous_visits"
  end

  create_table "foodpreferences", force: :cascade do |t|
    t.integer "attendee_id"
    t.boolean "gluten", default: false
    t.boolean "lactose", default: false
    t.string "other"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "vegan"
    t.boolean "vegetarian"
    t.index ["attendee_id"], name: "index_foodpreferences_on_attendee_id"
  end

  create_table "preferences", force: :cascade do |t|
    t.integer "attendee_id"
    t.boolean "alcohol", default: true, null: false
    t.boolean "sexa", default: false, null: false
    t.boolean "booklet", default: false, null: false
    t.string "medal", default: "no", null: false
    t.boolean "lunch", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "ticket_type"
    t.index ["attendee_id"], name: "index_preferences_on_attendee_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.string "remember_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["remember_token"], name: "index_users_on_remember_token"
  end

end
