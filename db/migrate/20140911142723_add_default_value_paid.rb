class AddDefaultValuePaid < ActiveRecord::Migration[5.0]
  def up
    Attendee.where(paid: nil).update_all(paid: false)
    change_column :attendees, :paid, :boolean, null: false, default: false
  end

  def down
    change_column :attendees, :paid, :boolean, null: true, default: nil
  end
end
