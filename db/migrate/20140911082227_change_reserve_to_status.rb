class ChangeReserveToStatus < ActiveRecord::Migration[5.0]
  def up
    add_column :attendees, :status, :string, default: 'attending'
    Attendee.all.each do |a|
      if a.reserve
        a.update_attribute :status, 'reserve'
      else
        a.update_attribute :status, 'attending'
      end
    end
    remove_column :attendees, :reserve
  end

  def down
    add_column :attendees, :reserve, :boolean
    Attendee.all.each do |a|
      if a.status == 'reserve'
        a.update_attribute :reserve, true
      else
        a.update_attribute :reserve, false
      end
    end
    remove_column :attendees, :status
  end
end
