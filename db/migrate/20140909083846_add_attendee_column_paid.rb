class AddAttendeeColumnPaid < ActiveRecord::Migration[5.0]
  def change
    add_column :attendees, :paid, :boolean
    Attendee.all.each do |a|
      a.update_attribute :paid, false
    end
  end
end
