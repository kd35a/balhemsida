class CreateFoodpreferences < ActiveRecord::Migration[5.0]
  def change
    create_table :foodpreferences do |t|
      t.belongs_to :attendee
      t.boolean :vegetarian, default: false
      t.boolean :vegan, default: false
      t.boolean :gluten, default: false
      t.boolean :lactose, default: false
      t.string :other
      t.integer :attendee_id
      t.timestamps
    end
  end
end
