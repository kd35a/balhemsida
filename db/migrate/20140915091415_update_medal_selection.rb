class UpdateMedalSelection < ActiveRecord::Migration[5.0]
  def up
    change_column :preferences, :medal, :string, null: false, default: 'no'
    add_column :attendees, :previous_visits, :string
  end

end
