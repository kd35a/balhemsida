class ChangeAlcoholDefaultValue < ActiveRecord::Migration[5.0]
  def up
    change_column :preferences, :alcohol, :boolean, default: true
  end

  def down
    change_column :preferences, :alcohol, :boolean, default: false
  end
end
