class AddGiverTicketType < ActiveRecord::Migration[5.0]
  def up
    add_column :preferences, :ticket_type, :integer
    Preference.all.each do |p|
      p.ticket_type = p.student ? :student : :non_student
      p.save!
    end
    remove_column :preferences, :student
  end
end
