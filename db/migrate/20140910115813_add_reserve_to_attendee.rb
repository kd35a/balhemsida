class AddReserveToAttendee < ActiveRecord::Migration[5.0]
  def change
    add_column :attendees, :reserve, :boolean
    Attendee.all.each do |a|
      a.update_attribute :reserve, false
    end
  end
end
