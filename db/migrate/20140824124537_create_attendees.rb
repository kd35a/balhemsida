class CreateAttendees < ActiveRecord::Migration[5.0]
  def change
    create_table :attendees do |t|
      t.string :name
      t.string :pnbr, null: false
      t.string :email, null: false
      t.string :companion
      t.string :title
      t.string :address
      t.string :refnbr, null: false
      t.string :phone
      t.timestamps
    end
  end
end
