class RollbackNoMeatOption < ActiveRecord::Migration[5.0]
  def change
    remove_column :foodpreferences, :veg
    add_column :foodpreferences, :vegan, :boolean
    add_column :foodpreferences, :vegetarian, :boolean
  end
end
