class UpdateNoMeatOption < ActiveRecord::Migration[5.0]
  #Add column veg, can have following options: vegan, vegetarian, false
  # default: false
  def self.up
    add_column :foodpreferences, :veg, :string, :default => 'false'
    remove_column :foodpreferences, :vegan
    remove_column :foodpreferences, :vegetarian
  end

  def self.down
    remove_column :foodpreferences, :veg
    add_column :foodpreferences, :vegan
    add_column :foodpreferences, :vegetarian
  end
end
