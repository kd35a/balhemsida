class CreatePreferences < ActiveRecord::Migration[5.0]
  def change
    create_table :preferences do |t|
      t.belongs_to :attendee
      t.boolean :student, null: false, default: true
      t.boolean :alcohol, null: false, default: false
      t.boolean :sexa, null: false, default: false
      t.boolean :booklet, null: false, default: false
      t.boolean :medal, null: false, default: false
      t.boolean :lunch, null: false, default: false
      t.integer :attendee_id
      t.timestamps
    end
  end
end
