# Balhemsida
Blekingska Nationens balhemsida anno domini 2014

# Development and releasing/deploying

## Main branches
The project consist of two main branches, `master` and `release`.

**`master`**: This is the main branch that tracks what should come in a future
release.
**`release`**: This branch always points to the most current release. This
branch should only be updated with commits already present in `master`. More on
that later on.

## Development
Apart from these there are also issue-branches. All work on the website should
have an issue related to it. This issue is then used to create an issue-branch
named after the issue id, for example `issue-123`. Here the developer does
his/hers work.

When the developer feels that the issue has been fixed, a pull request is
created. This pull request should be reviewed and approved by at least one other
developer before merged.

Mergeing is preceded by a rebase on the current master, and, if necessary,
squashing of commits to create a simple and easily understandable history. All
commits should reference the issue addressed, and have a
[proper commit message](http://chris.beams.io/posts/git-commit/). For example:

```
Add support for lazors and kittens

By adding foo we can now support lazors, and as a consequence also kittens.

Refs #1337
```

When the issue-branch looks proper, `master` can merge the branch using
fast-forwarding.

Example of workflow:
```bash
git checkout master
git pull origin
git checkout -b issue-123
echo 'Hello, world!' > foobar.txt
git add foobar.txt
git commit # "Add foobar with greating message\n\nRefs #123"
git push -u origin issue-123
# Create pull request, get it approved
git checkout master
git pull origin
git checkout issue-123
git rebase master
git push --force origin issue-123 # Do this with great care...
git checkout master
git merge --ff-only issue-123
git push origin master
```

## Release/deploy
When there are new features added to `master` and it is time for a new release,
the `release`-branch is checked out, and fast-forwarded to master. The new HEAD
is tagged with a release number kind of following
[semantic versioning](http://semver.org/). After this the tag and `release` are
pushed to origin, and thereafter Heroku where it is running.

Example of workflow:
```bash
git checkout master
git pull --all
git checkout release
git pull --all
git merge --ff-only master
git tag 1.2.3
git push --tags origin release
git push --tags heroku release:master # Heroku deploys the master branch, therefor the renaming
```


# License
Balhemsida is released under the MIT License.
