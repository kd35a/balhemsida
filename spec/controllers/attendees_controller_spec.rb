require 'rails_helper'

RSpec.describe AttendeesController, :type => :controller do
  before :each do
    User.delete_all
    Attendee.delete_all

    @user = User.create(name: 'admin', email: 'ad@admin.test', password: 'admin')
    Attendee.create(name: 'test', email: 't1@test.test', pnbr: '1234567890',
      phone: '123', address: 'street',
      preference_attributes: { student: 'true', medal: 'yes', alcohol: 'true', lunch: 'true', booklet: 'true'},
      foodpreference_attributes: {})
    Attendee.create(name: 'test2', email: 't2@test.test', pnbr: '1234567891',
      phone: '123', address: 'street',
      preference_attributes: { student: 'true', medal: 'yes', alcohol: 'true', lunch: 'true', booklet: 'true'},
      foodpreference_attributes: {})
    Attendee.create(name: 'test3', email: 't3@test.test', pnbr: '1234567892',
      phone: '123', address: 'street',
      preference_attributes: { student: 'true', medal: 'yes', alcohol: 'true', lunch: 'true', booklet: 'true'},
      foodpreference_attributes: {})
    sign_in(@user)
  end

  render_views

  describe "edit_mark_as_paid" do
    it "must be logged in" do
      sign_out
      get 'edit_mark_as_paid', {:attendee_ids => [Attendee.first.id]}
      expect(response).to redirect_to '/signin'
      post 'update_mark_as_paid', {:attendee_ids => [Attendee.first.id]}
      expect(response).to redirect_to '/signin'
      expect(Attendee.where(paid: false, status: 'attending').count).to eq(3)
    end

    it "no attendees selected" do
      get :edit_mark_as_paid
      expect(response).to redirect_to('/admin')
      expect(Attendee.where(paid: false, status: 'attending').count).to eq(3)
    end

    it "one attendee selected" do
      get 'edit_mark_as_paid', {:attendee_ids => [Attendee.first.id]}
      expect(response).to render_template :edit_mark_as_paid
      mails_sent_before_update = ActionMailer::Base.deliveries.count
      post 'update_mark_as_paid', {:attendee_ids => [Attendee.first.id]}
      mails_sent_after_update = ActionMailer::Base.deliveries.count
      expect(mails_sent_after_update - mails_sent_before_update).to eq(1) #check if mail is sent
      expect(response).to render_template :update_mark_as_paid
      expect(Attendee.where(paid: false, status: 'attending').count).to eq(2)
    end

    it "many attendees selected" do
      attendees = [Attendee.first.id, Attendee.second.id]
      get 'edit_mark_as_paid', {:attendee_ids => attendees}
      expect(response).to render_template :edit_mark_as_paid
      mails_sent_before_update = ActionMailer::Base.deliveries.count
      post 'update_mark_as_paid', {:attendee_ids => attendees}
      mails_sent_after_update = ActionMailer::Base.deliveries.count
      expect(mails_sent_after_update - mails_sent_before_update).to eq(2) #check if mail is sent
      expect(response).to render_template :update_mark_as_paid
      expect(Attendee.where(paid: false, status: 'attending').count).to eq(1)
    end
  end
end
