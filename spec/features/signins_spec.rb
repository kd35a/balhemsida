require 'rails_helper'

feature "Signins", :type => :feature do
  describe "the signin/signout process", :type => :feature do
    before :each do
      User.create(:name => 'admin', email: 'ad@admin.test', password: 'admin')
    end

    it "signs me in" do
      visit '/signin'
      fill_in 'session_name', :with => 'admin'
      fill_in 'session_password', :with => 'admin'
      click_button 'Logga in'
      expect(page).to have_content 'Admin'
    end

    it "does not signs me in, wrong name" do
      visit '/signin'
      fill_in 'session_name', :with => 'wrong'
      fill_in 'session_password', :with => 'admin'
      click_button 'Logga in'
      expect(page).to have_content 'Ogiltligt användarnamn eller lösenord'
    end

    it "does not sign me in, wrong password" do
      visit '/signin'
      fill_in 'session_name', :with => 'admin'
      fill_in 'session_password', :with => 'wrong'
      click_button 'Logga in'
      expect(page).to have_content 'Ogiltligt användarnamn eller lösenord'
    end

    it "should log me out" do
      visit '/signin'
      fill_in 'session_name', :with => 'admin'
      fill_in 'session_password', :with => 'admin'
      click_button 'Logga in'
      expect(page).to have_content 'Admin'
      click_link 'Logga ut'
      expect(page).to have_content 'Logga in'
    end
  end
end
