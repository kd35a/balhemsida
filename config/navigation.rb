# -*- coding: utf-8 -*-
# Configures your navigation
SimpleNavigation::Configuration.run do |navigation|
  # Specify the class that will be applied to active navigation items.
  # Defaults to 'selected' navigation.selected_class = 'your_selected_class'

  # Define the primary navigation
  navigation.items do |primary|
    primary.item :home, t(:home), url_for(controller: 'static_pages', action: 'home')
    primary.item :history, t(:history), url_for(controller: 'static_pages', action: 'history')
    primary.item :food_menu, t(:food_menu), url_for(controller: 'static_pages', action: 'menu')
    # primary.item :program, t(:program), url_for(controller: 'static_pages', action: 'program')
    primary.item :medalj, t(:medal), url_for(controller: 'static_pages', action: 'medalj')
    primary.item :faq, t(:faq), url_for(controller: 'static_pages', action: 'faq')
    primary.item :signup, t(:signup), url_for(controller: 'attendees', action: 'new'),
      if: proc {
        Time.zone.parse("2019-09-16").past? and Time.zone.parse("2019-11-24").future?
      }
    primary.item :attendees, t(:attendees), url_for(controller: 'attendees', action: 'list_paid'),
      if: proc { Time.zone.parse("2019-09-16").past? }
    primary.item :admin, t(:admin),
      url_for(controller: 'admin', action: 'index'),
      if: proc { signed_in? }

    # Add an item which has a sub navigation (same params, but with block)
    #primary.item :key_2, 'name', url, options do |sub_nav|
      # Add an item to the sub navigation (same params again)
      #sub_nav.item :key_2_1, 'name', url, options
    #end

    # You can also specify a condition-proc that needs to be fullfilled to display an item.
    # Conditions are part of the options. They are evaluated in the context of the views,
    # thus you can use all the methods and vars you have available in the views.
    #primary.item :key_3, 'Admin', url, class: 'special', if: -> { current_user.admin? }
    #primary.item :key_4, 'Account', url, unless: -> { logged_in? }

    # you can also specify html attributes to attach to this particular level
    # works for all levels of the menu
    # primary.dom_attributes = {id: 'menu-id', class: 'menu-class'}

    # You can turn off auto highlighting for a specific level
    # primary.auto_highlight = false
  end
end
