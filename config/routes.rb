Rails.application.routes.draw do

  get '/signin' => 'sessions#new'
  post '/signin' => 'sessions#create'
  delete '/signout' => 'sessions#destroy'

  get '/admin' => 'admin#index'
  get '/admin/export_attendees' => 'admin#export_attendees'
  get '/admin/edit_mark_as_paid' => 'attendees#edit_mark_as_paid'
  get '/admin/list_all' => 'attendees#list_all'
  get '/admin/edit_attendee' => 'attendees#edit'
  patch '/admin/update_attendee' => 'attendees#update'
  post '/admin/update_mark_as_paid' => 'attendees#update_mark_as_paid'

  scope '(:locale)', :locale => /en|sv/, defaults: {locale: 'sv'} do
    root 'static_pages#home'
    get '/history' => 'static_pages#history'
    get '/menu' => 'static_pages#menu'
    get '/program' => 'static_pages#program'
    get '/medalj' => 'static_pages#medalj'
    get '/faq' => 'static_pages#faq'
    get '/anmalan' => 'attendees#new'
    get '/anmalda' => 'attendees#list_paid'
    post '/anmalan' => 'attendees#create'
    resources :attendees, only: [:show]
  end

end
