# -*- coding: utf-8 -*-
SimpleNavigation::Configuration.run do |navigation|
  navigation.items do |primary|
    primary.dom_class = 'language'
    primary.item :in_english, t(:in_english),
      url_for(locale: I18n.default_locale == I18n.locale ? 'en' : I18n.default_locale),
      if: proc { not signed_in? }
    primary.item :sign_out, t(:sign_out),
      url_for(controller: 'sessions', action: 'destroy'), method: :delete,
      if: proc { signed_in? }
  end
end
