# Managing Heroku

Install [Heroku Toolbelt](https://toolbelt.heroku.com) to manage the servers.

## List applications (servers)
```sh
heroku apps
```

## Get a rails console
```sh
heroku run --app desolate-headland-7399 rails c
```
