# Add or change user

## Update user password
```sh
$ bundle exec rails c
u = User.find(1)
u.password = "changeme"
u.save
```

## Create new user
```sh
$ bundle exec rails c
u = User.create(name: "Foo Bar", email: "foo@example.com", password: "changeme")
u.save
```
